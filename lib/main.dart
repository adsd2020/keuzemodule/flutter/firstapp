import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScore = 0;
  final _questions = const [
    {
      'questionText': 'What\'s your favorit color?',
      'answers': [
        {
          'text': 'Black',
          'score': 4,
        },
        {
          'text': 'Red',
          'score': 3,
        },
        {
          'text': 'Green',
          'score': 2,
        },
        {
          'text': 'White',
          'score': 1,
        },
      ],
    },
    {
      'questionText': 'What\'s your favorit food?',
      'answers': [
        {
          'text': 'Pizza',
          'score': 3,
        },
        {
          'text': 'French fries',
          'score': 2,
        },
        {
          'text': 'Pasta',
          'score': 1,
        },
      ],
    },
    {
      'questionText': 'What\'s your favorit season?',
      'answers': [
        {
          'text': 'Spring',
          'score': 1,
        },
        {
          'text': 'Summer',
          'score': 2,
        },
        {
          'text': 'Winter',
          'score': 3,
        },
        {
          'text': 'Fal',
          'score': 4,
        },
      ],
    },
  ];

  void resetQuiz() {
    setState(() {
      _totalScore = 0;
      _questionIndex = 0;
    });
  }

  void answerQuestion(int score) {
    _totalScore += score;
    if (_questionIndex < _questions.length) {
      setState(() {
        _questionIndex++;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My first Flutter app'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                questions: _questions,
                answerQuestion: answerQuestion,
                questionIndex: _questionIndex,
              )
            : Result(_totalScore, resetQuiz),
      ),
    );
  }
}
