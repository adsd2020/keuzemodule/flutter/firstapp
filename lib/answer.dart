import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final String answerText;
  final VoidCallback clickHandler;

  const Answer({Key? key, required this.answerText, required this.clickHandler})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width:double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: ElevatedButton(
        child: Text(answerText),
        style: ElevatedButton.styleFrom(
          primary: Colors.blue.shade900,
          minimumSize: Size.fromHeight(40),
        ),
        onPressed: clickHandler,
      ),
    );
  }
}
