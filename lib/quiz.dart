import 'package:flutter/material.dart';

import './question.dart';
import './raisd_answer.dart';
import './answer.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final Function answerQuestion;
  final int questionIndex;

  const Quiz({
    Key? key,
    required this.questions,
    required this.answerQuestion,
    required this.questionIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Question(
            questionText: questions[questionIndex]['questionText'] as String),
        Column(
          children: [
            ...(questions[questionIndex]['answers'] as List<Map<String,Object>>)
                .map((answer) {
              return Answer(
                answerText: answer['text'] as String,
                clickHandler: () => answerQuestion(answer['score']),
              );
            }),
          ],
        ),
      ],
    );
  }
}
