import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final VoidCallback resetHandler;

  const Result(this.resultScore, this.resetHandler);

  String get resultPhrase {
    return 'Your score is: ' + resultScore.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            'You did it! ',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 35,
                fontFamily: 'Delight Candles'),
          ),
          Text(
            resultPhrase,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 35,
                fontFamily: 'Delight Candles'),
          ),
          FlatButton(
            onPressed: resetHandler,
            child: Text('Try it again (Old)'),
            textColor: Colors.blue.shade900,
          ),
          TextButton(
            onPressed: resetHandler,
            child: Text('Try it again'),
            style: TextButton.styleFrom(primary: Colors.blue.shade900),
          ),
          OutlineButton(
            onPressed: resetHandler,
            child: Text('Try it again (Old)'),
            textColor: Colors.blue.shade900,
            borderSide: BorderSide(color: Colors.orange),
          ),
          OutlinedButton(
            onPressed: resetHandler,
            child: Text('Try it again'),
            style: OutlinedButton.styleFrom(
              primary: Colors.blue.shade900,
              side: BorderSide(color: Colors.orange.shade900)
            ),
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
    );
  }
}
