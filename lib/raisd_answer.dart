import 'package:flutter/material.dart';

class AnswerButton extends StatelessWidget {
  final String answerText;
  final VoidCallback clickHandler;

  const AnswerButton(
      {Key? key, required this.answerText, required this.clickHandler})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: RaisedButton(
        child: Text(answerText),
        onPressed: clickHandler,
        color: Colors.blue.shade900,
        textColor: Colors.white,
      ),
    );
  }
}
